var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var jwt = require('jsonwebtoken');
var morgan = require('morgan');
var cookieSession = require('cookie-session')
var replace = require('./routes/utils/replace');
var mongoose = require('mongoose');
var minifyHTML = require('express-minify-html');
var Ticker = require('./routes/models/ticket');
// admin
var admin_dashboard = require('./routes/admin/dashboard');
var admin_accounts = require('./routes/admin/accounts');
var admin_students = require('./routes/admin/students');
var edit_student_api_admin = require('./routes/admin_api/edit_student');
var edit_account_student_api_admin = require('./routes/admin_api/student_editor');
var create_student_api_admin = require('./routes/admin_api/create_student');
var edit_appointment_api_admin = require('./routes/admin_api/edit_appointment');
var delete_account_api_admin = require('./routes/admin_api/delete_account');
var edit_account_api_admin = require('./routes/admin_api/edit_account');
var get_all_students_api_admin = require('./routes/admin_api/get_all_students');
var delete_student_api_admin = require('./routes/admin_api/delete_student');
var get_all_classes_api_admin = require('./routes/admin_api/get_all_classes')
var get_all_years_api_admin = require('./routes/admin_api/get_all_years')
// main
var index = require('./routes/index');
var database = require('./routes/database/connect');
var dashboard = require('./routes/main/dashboard');
var account = require('./routes/main/account');
var view_appointment = require('./routes/main/view_appointment');
var view_report = require('./routes/main/view_report');
var view_profile = require('./routes/main/view_profile');
var logout = require('./routes/main/logout');
var children = require('./routes/main/children');
var tickets = require('./routes/main/tickets');
var view_student = require('./routes/main/view_student');
var view_ticket = require('./routes/main/view_ticket');
// mongoose implemented
var create_account = require('./routes/auth/create_account');
var edit_account_api = require('./routes/api/edit_account')
var forgot_password = require('./routes/auth/forgot_password');
var forgot_password_reset = require('./routes/auth/forgot_password_reset');
var get_appointment_api = require('./routes/api/get_appointment');
var create_ticket_api = require('./routes/api/create_ticket');
var create_appointment_api = require('./routes/api/create_appointment');
var create_account_api = require('./routes/api/create_account');
var login_account_api = require('./routes/auth/login_account');
var get_year_data = require('./routes/api/get_year_data');
var get_student_data = require('./routes/api/get_student_data')
var delete_ticket_api = require('./routes/api/delete_ticket');
var delete_appointment_api = require('./routes/api/delete_appointment');
var delete_appointment_api_admin = require('./routes/admin_api/delete_appointment');
var get_class_by_uuid = require('./routes/api/get_class_by_uuid');
var get_group_by_id = require('./routes/api/get_group_by_id');
var get_group_by_email = require('./routes/api/get_group_by_email');
var get_group_by_uuid = require('./routes/api/get_group_by_uuid');
var get_groups = require('./routes/api/get_groups');
var get_user_data = require('./routes/api/get_user_data');
var check_date_time_avaliable = require('./routes/api/check_date_time_avaliable');
var get_teachers = require('./routes/api/get_teachers');
var error = require('./routes/error');
var middleware = require('./middleware');
var flash = require('connect-flash');
var app = express();

mongoose.connect(database.url);
var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function() {
  // we're connected!
});

app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');
var minify = require('express-minify');
app.use(minify());
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(bodyParser.json({limit: '5mb'}));
app.use(bodyParser.urlencoded({ extended: true, limit: '5mb' }));
app.use(express.static(path.join(__dirname, 'public')));
app.use('/public', express.static('public'))
app.use(cookieParser());
app.use(cookieSession({
  name: 'session',
  keys: ["lirtt£@$£@$£$!@Dsda345£$$£%£$%@£dDFFgdfgf$%^%$d", "fdsf34charlie@$342£@sa@we4097@$$@&*@$£$!@Dsdad"],
  maxAge: 24 * 60 * 60 * 1000 // 24 hours
}));
app.use(flash());
app.use(morgan('dev'));
app.use(minifyHTML({
    override:      true,
    exception_url: false,
    htmlMinifier: {
        removeComments:            true,
        collapseWhitespace:        true,
        collapseBooleanAttributes: true,
        removeAttributeQuotes:     true,
        removeEmptyAttributes:     true,
        minifyJS:                  true
    }
}));

// RESTful APIs -----------------------------------

app.use('/admin_api', middleware.admin_authentication);
app.use('/admin_api', delete_appointment_api_admin);
app.use('/admin_api', edit_appointment_api_admin);
app.use('/admin_api', edit_student_api_admin);
app.use('/admin_api', edit_account_api_admin);
app.use('/admin_api', edit_account_student_api_admin);
app.use('/admin_api', delete_account_api_admin);
app.use('/admin_api', get_all_students_api_admin);
app.use('/admin_api', get_all_years_api_admin);
app.use('/admin_api', get_all_classes_api_admin);
app.use('/admin_api', delete_student_api_admin);
app.use('/admin_api', create_student_api_admin);
// NORMAL USER API -------------------
app.use('/api', middleware.authentication);
app.use('/api', get_user_data);
app.use('/api', get_class_by_uuid);
app.use('/api', get_group_by_email);
app.use('/api', get_group_by_id);
app.use('/api', get_group_by_uuid);
app.use('/api', delete_appointment_api);
app.use('/api', delete_ticket_api);
app.use('/api', get_appointment_api);
app.use('/api', check_date_time_avaliable);
app.use('/api', get_year_data);
app.use('/api', get_student_data);
app.use('/api', edit_account_api);
app.use('/api', create_ticket_api);
app.use('/api', get_teachers);
app.use('/api', create_appointment_api);
app.use('/api', get_groups);
app.use('/auth_api/login_account', login_account_api);
// -------------------------------------------------------

// Non RESTful APIs -----------------------
app.use('/auth_api/create_account', create_account_api);
// ----------------------------------------

// Rendering -----------------------
app.use('/admin/dashboard', admin_dashboard);
app.use('/admin/accounts', admin_accounts)
app.use('/admin/students', admin_students)
app.use('/main/view_appointment/', view_appointment);
app.use('/main/view_ticket/', view_ticket);
app.use('/main/view_profile/', view_profile);
app.use('/main/view_student/', view_student);
app.use('/main/view_report/', view_report);
app.use('/main/tickets/', tickets);
app.use('/main/children/', children);
app.use('/main/dashboard', dashboard);
app.use('/main/account', account);
app.use('/main/logout', logout);
app.use('/auth/create_account', create_account);
app.use('/auth/forgot_password', forgot_password);
app.use('/auth/forgot_password_reset', forgot_password_reset);
app.use('/error', error);
app.use('/', index);
// ---------------------------------
// catch 404 and forward to error handlerrr
/*app.use(function(req, res, next) {
  console.log(req.path);
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});*/

var port = process.env.port || 3003;

var server = app.listen(port,function(){
  console.log("Express is running on port " + port);
  var io = require('socket.io')(server);
  var Ticket = require('./routes/models/ticket');
  var ObjectID = require('mongodb').ObjectID;

  io.on('connection', function (socket) {
    var sockets = require("./sockets")(socket, io);
    socket.on('get_messages', sockets.onMessages);
    socket.on('join_room_request', sockets.joinRoom);
    socket.on('new_message', sockets.newMessage);
  });
});
