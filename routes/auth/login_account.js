var express = require('express');
var router = express.Router();
var utils = require('../utils/utils');
var bcrypt = require('bcrypt-nodejs');

router.post('/',function(req,res){
  utils.isLoggedIn(req,function(call) {
    if(call == false) {
      res.status(200)
      email = req.body.email;
      password = req.body.password;
      console.log("User requested login account: " + email);
      if(email == "" || password == "") {
        req.flash('message', 'Please fill all fields in to sign-up correctly.')
        req.flash('message_type', 'error')
        res.redirect('/');
      } else {
        utils.loginCheck(email, password, function(result) {
          if(result.success == true) {
            req.session.uuid = result._id;
            res.json(result);
          } else {
            res.send(result);
          }
        });
      }
    } else {
      res.redirect('/main/dashboard/false');
    }
  });
});

module.exports = router;
