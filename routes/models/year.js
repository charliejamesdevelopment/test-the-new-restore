var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var yearSchema = new Schema({
  name: { type: String, required: true },
  created_at: Date,
  updated_at: Date
});

yearSchema.pre('save', function(next) {
  // get the current date
  var currentDate = new Date();

  // change the updated_at field to current date
  this.updated_at = currentDate;

  // if created_at doesn't exist, add to that field
  if (!this.created_at)
    this.created_at = currentDate;

  next();
});
yearSchema.set('collection', 'years');

var User = mongoose.model('Year', yearSchema);

// make this available to our users in our Node applications
module.exports = User;
