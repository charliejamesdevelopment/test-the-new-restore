var mongoose = require('mongoose');
var bcrypt = require('bcrypt-nodejs');
var Schema = mongoose.Schema,
    SALT_WORK_FACTOR = 10;

var userSchema = new Schema({
  name: { type: String, required: true},
  email: { type: String, required: true, unique: true },
  password: { type: String, required: true },
  students: { type: Array, required: false },
  bio: { type: String, required: false, default: "Hey, change this biography description in the 'account' page!" },
  group: { type: Number, required: true},
  picture_url: { type: String, required: false},
  created_at: Date,
  updated_at: Date
});

userSchema.pre('save', function(next) {
  var currentDate = new Date();
  this.updated_at = currentDate;
  if (!this.created_at)
    this.created_at = currentDate;

  var user = this;
  if (!user.isModified('password')) return next();
  user.password = bcrypt.hashSync(user.password);

  next();
});

userSchema.methods.comparePassword = function (passw, cb) {
    bcrypt.compare(passw, this.password, function (err, isMatch) {
        if (err) {
            return cb(err);
        }
        cb(null, isMatch);
    });
};

userSchema.set('collection', 'accounts');

var User = mongoose.model('User', userSchema);

// make this available to our users in our Node applications
module.exports = User;
