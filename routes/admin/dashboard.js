var express = require('express');
var router = express.Router();
var utils = require('../utils/utils');
var config = require('../../config.js');
var appointment_utils = require('../utils/appointments.js');

router.get('/',function(req,res){
  utils.isLoggedIn(req,function(call) {
    if(call == false) {
      res.redirect('/');
    } else {
      utils.getUserData(req.session.uuid, function(obj) {
        if(obj == 1) {
          console.log("Couldn't find data for " + req.session.uuid);
        } else {
          utils.getGroupByUUID(req.session.uuid, function(g) {
            utils.getAllAppointments(function(appointments) {
              if(g.admin == false) {
                req.flash('error', 'Something went wrong...')
                res.redirect("/error");
              } else {
                res.render('admin/dashboard', {
                  getStatus: appointment_utils.getStatus,
                  getAppointmentStatuses: appointment_utils.getAppointmentStatuses,
                  admin: g.admin,
                  times: config.appointment.times,
                  types: config.appointment.types,
                  stats: appointment_utils.getAppointmentStatuses(),
                  user: obj,
                  group: g,
                  appointments: appointments,
                  active: "All Appointments",
                });
              }
            });
          });
        }
      });
    }
  });
});

module.exports = router;
