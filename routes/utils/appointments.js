getAppointmentStatuses = function() {
  appointmentStatuses = [
    {
      "name" : "Pending",
      "class" : "pending",
      "description" : "Appointment has not yet been accepted or denied by relevant staff member.",
      "id" : 0,
      "color" : "yellow"
    },
    {
      "name" : "Accepted",
      "class" : "accepted",
      "description" : "Appointment has been accepted by relevant staff member.",
      "id" : 1,
      "color" : "green"
    },
    {
      "name" : "Closed",
      "class" : "closed",
      "description" : "Appointment has been fulfilled and dealt with.",
      "id" : 2,
      "color" : "#ccc"
    },
    {
      "name" : "Denied",
      "class" : "denied",
      "description" : "Appointment has been denied by relevant staff member.",
      "id" : 3,
      "color" : "#333"
    },
    {
      "name" : "Cancelled",
      "class" : "cancelled",
      "description" : "Appointment has been cancelled by either parent or teacher.",
      "id" : 4,
      "color" : "red"
    }
  ]
  return appointmentStatuses;
}

getStatus = function(code) {
  statuses = getAppointmentStatuses();
  _status = "";
  switch(code) {
    case 0:
      _status = statuses[0];
      break;
    case 1:
      _status = statuses[1];
      break;
    case 2:
      _status = statuses[2];
      break;
    case 3:
      _status = statuses[3];
      break;
    case 4:
      _status = statuses[4];
      break;
    default:
      _status = "N/A";
      break;
  }
  return _status;
}

module.exports = {
  getAppointmentStatuses: getAppointmentStatuses,
  getStatus: getStatus
}
