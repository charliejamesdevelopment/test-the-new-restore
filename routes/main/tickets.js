var express = require('express');
var router = express.Router();
var utils = require('../utils/utils');
var moment = require('moment');
var config = require('../../config.js');
var appointment_utils = require('../utils/appointments.js');

router.get('/:show_hidden',function(req,res){
  utils.isLoggedIn(req,function(call) {
    res.status(200)
    if(call == false) {
      res.redirect('/');
    } else {
      utils.getUserData(req.session.uuid, function(obj) {
        if(obj == 1) {
          console.log("Couldn't find data for " + req.session.uuid);
        } else {
          utils.getGroupByUUID(req.session.uuid, function(g) {
            utils.getTickets(req.session.uuid, req.params.show_hidden, function(tickets) {
              var button_text = "Show Closed Tickets"
              var button_url = "/main/tickets/true";
              if(req.params.show_hidden == "true") {
                var button_text = "Hide Closed Tickets"
                var button_url = "/main/tickets/false";
              }
              utils.getChildrenByUUID(req.session.uuid, function(data) {
                res.render('main/tickets', {
                  getStatus: appointment_utils.getStatus,
                  getAppointmentStatuses: appointment_utils.getAppointmentStatuses,
                  times: config.appointment.times,
                  types: config.appointment.types,
                  admin: g.admin,
                  user: obj,
                  group: g,
                  data: data,
                  active: "My Tickets",
                  tickets: tickets,
                  moment: moment,
                  button_text: button_text,
                  button_url: button_url
                });
              });
            });
          });
        }
      });
    }
  });
});

module.exports = router;
