var express = require('express');
var router = express.Router();
var utils = require('../utils/utils');
var bcrypt = require('bcrypt-nodejs');

router.get('/get_teachers',function(req,res){
    utils.getTeachers(function(call) {
      res.send(call);
    });
});

module.exports = router;
