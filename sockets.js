var Ticket = require("./routes/models/ticket");
var ObjectID = require('mongodb').ObjectID;

module.exports = function(socket, io){
  return {
    onMessages : function(data) {
      ticket = data.ticket;
      Ticket.find({_id : new ObjectID(data.ticket)}, function(err, results) {
        if(err) {
          socket.emit('error', {"message" : "Something went wrong! (err: 3)"});
        } else {
          ticket = results[0];
          socket.emit('get_messages', {"messages" : ticket.messages});
        }
      });
    },
    joinRoom : function(data) {
      socket.join(data.room);
    },
    newMessage : function(data) {
      if(data.message != "") {
        Ticket.find({_id : new ObjectID(data.ticket)}, function(err, results) {
          if(err) {
            socket.emit('error', {"message" : "Something went wrong! (err: 1)"});
          } else {
            ticket = results[0];
            arr = ticket.messages;
            var message = {id: arr.length+1, username: data.username, message: data.message, date: new Date()};
            arr.push(message);
            Ticket.update({_id : new ObjectID(data.ticket)}, {$set: {"messages" : arr}}, function(err, doc) {
              if(err) {
                socket.emit('error', {"message" : "Something went wrong (err: 2)!"});
              } else {
                socket.emit('success', {"message" : "Message has been sent!"});
                io.to(data.ticket).emit('broadcast_message',message);
              }
            });
          }
        });
      } else {
        socket.emit('error', {"message" : "Please input a message!"});
      }
    }
  }
};
